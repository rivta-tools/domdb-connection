package se.rivta.domdb.connection.core

import spock.lang.Specification


class RoutingSpec extends Specification {
    void testEqualsAndHashCode_equalObjects() {
        setup:
        def serviceComponent = new ServiceComponent(id: 3)
        def serviceContract = new ServiceContract(id: 4, namespace: "urn:riv:crm")
        def logicalAddress = new LogicalAddress(id: 9)
        def tak = new Tak(id: 2)
        def first = new Routing(serviceComponent: serviceComponent, serviceContract: serviceContract, logicalAddress: logicalAddress, tak: tak)
        def second = new Routing(serviceComponent: serviceComponent, serviceContract: serviceContract, logicalAddress: logicalAddress, tak: tak)

        expect:
        first.equals(second)
        first.hashCode() == second.hashCode()
    }

    void testEqualsAndHashCode_differentServiceComponent() {
        setup:
        def serviceComponent1 = new ServiceComponent(id: 3)
        def serviceComponent2 = new ServiceComponent(id: 4)
        def serviceContract = new ServiceContract(id: 4, namespace: "urn:riv:crm")
        def logicalAddress = new LogicalAddress(id: 9)
        def tak = new Tak(id: 2)
        def first = new Routing(serviceComponent: serviceComponent1, serviceContract: serviceContract, logicalAddress: logicalAddress, tak: tak)
        def second = new Routing(serviceComponent: serviceComponent2, serviceContract: serviceContract, logicalAddress: logicalAddress, tak: tak)

        expect:
        !first.equals(second)
        first.hashCode() != second.hashCode()
    }

    void testEqualsAndHashCode_differentTak() {
        setup:
        def serviceComponent = new ServiceComponent(id: 3)
        def serviceContract = new ServiceContract(id: 4, namespace: "urn:riv:crm")
        def logicalAddress = new LogicalAddress(id: 9)
        def tak1 = new Tak(id: 1)
        def tak2 = new Tak(id: 2)
        def first = new Routing(serviceComponent: serviceComponent, serviceContract: serviceContract, logicalAddress: logicalAddress, tak: tak1)
        def second = new Routing(serviceComponent: serviceComponent, serviceContract: serviceContract, logicalAddress: logicalAddress, tak: tak2)

        expect:
        !first.equals(second)
        first.hashCode() != second.hashCode()
    }

    void testEqualsAndHashCode_differentServiceContract() {
        setup:
        def serviceComponent = new ServiceComponent(id: 3)
        def serviceContract1 = new ServiceContract(id: 4, namespace: "urn:riv:crm")
        def serviceContract2 = new ServiceContract(id: 5, namespace: "urn:riv:followup")
        def logicalAddress = new LogicalAddress(id: 9)
        def tak = new Tak(id: 2)
        def first = new Routing(serviceComponent: serviceComponent, serviceContract: serviceContract1, logicalAddress: logicalAddress, tak: tak)
        def second = new Routing(serviceComponent: serviceComponent, serviceContract: serviceContract2, logicalAddress: logicalAddress, tak: tak)

        expect:
        !first.equals(second)
        first.hashCode() != second.hashCode()
    }

    void testEqualsAndHashCode_differentLogicalAddress() {
        setup:
        def serviceComponent = new ServiceComponent(id: 3)
        def serviceContract = new ServiceContract(id: 4, namespace: "urn:riv:crm")
        def logicalAddress1 = new LogicalAddress(id: 9)
        def logicalAddress2 = new LogicalAddress(id: 15)
        def tak = new Tak(id: 2)
        def first = new Routing(serviceComponent: serviceComponent, serviceContract: serviceContract, logicalAddress: logicalAddress1, tak: tak)
        def second = new Routing(serviceComponent: serviceComponent, serviceContract: serviceContract, logicalAddress: logicalAddress2, tak: tak)

        expect:
        !first.equals(second)
        first.hashCode() != second.hashCode()
    }
}
