#!/usr/bin/env groovy

package groovy

@Grapes([
        @Grab(group='se.rivta.domdb.connection', module='domdb-connection-core', version='[1.2,)'),
        @Grab(group='org.hibernate', module='hibernate-entitymanager', version='4.3.7.Final'),
        @GrabExclude('xml-apis:xml-apis'), // Kills EntityManagerFactory with DocumentBuilderFactory error
        @Grab('mysql:mysql-connector-java:5.1.34'),
        @GrabConfig(systemClassLoader = true)
])

import javax.persistence.EntityManager
import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence

def configFileName = "${System.getenv("domdb_config_dir")}/connections-jpa.properties"
def configStream = new FileInputStream(configFileName)
def jpaProperties = new Properties()
jpaProperties.load(configStream)
jpaProperties["hibernate.hbm2ddl.auto"] = "update"

EntityManagerFactory factory = Persistence.createEntityManagerFactory("datasource-connections", jpaProperties)

EntityManager manager = factory.createEntityManager()
manager.close()
factory.close()

println("Success!")