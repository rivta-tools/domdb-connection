package se.rivta.domdb.connection.core

import javax.persistence.*


@Entity
@Table(
        indexes = @Index(columnList="address, tak_id", unique = true)
)
class LogicalAddress {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // db generated id
    int id

    @Column(nullable = false)
    String address

    @Column(columnDefinition = "text")
    String description

    @ManyToOne(optional = false)
    Tak tak
}
