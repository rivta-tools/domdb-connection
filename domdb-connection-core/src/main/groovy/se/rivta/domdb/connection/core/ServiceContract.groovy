package se.rivta.domdb.connection.core

import javax.persistence.*


@Entity(name = "connection.ServiceContract") // Solves conflict with SeviceContract in domain package
@Table(
        indexes = @Index(columnList = "namespace, tak_id", unique = true)
)
class ServiceContract {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // db generated id
    int id

    @Column(nullable = false)
    String namespace

    @ManyToOne(optional = false)
    Tak tak

    @OneToMany(mappedBy = "serviceContract")
    Set<Routing> routes

    @OneToMany(mappedBy = "serviceContract")
    Set<CallAuthorization> authorizations
}
