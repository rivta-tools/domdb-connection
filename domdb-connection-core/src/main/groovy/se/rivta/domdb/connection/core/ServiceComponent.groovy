package se.rivta.domdb.connection.core

import javax.persistence.*


@Entity
@Table(
        indexes = @Index(columnList = "hsaId, tak_id", unique = true)
)
class ServiceComponent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // db generated id
    int id

    @Column(nullable = false)
    String hsaId

    @Column(columnDefinition = "text")
    String description

    @ManyToOne(optional = false)
    Tak tak
}
