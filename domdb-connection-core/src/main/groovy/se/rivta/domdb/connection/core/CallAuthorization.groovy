package se.rivta.domdb.connection.core

import javax.persistence.*


@Entity
class CallAuthorization implements Serializable {
    @Id
    @ManyToOne(optional = false)
    Tak tak

    @Id
    @ManyToOne(optional = false)
    ServiceContract serviceContract

    @Id
    @ManyToOne(optional = false)
    ServiceComponent serviceComponent

    @Id
    @ManyToOne(optional = false)
    LogicalAddress logicalAddress

    /* equals and hashCode overridden to be able to use objects in hashsets */

    @Override
    public boolean equals(Object other) {
        if (!(other instanceof CallAuthorization))
            return false;

        return (tak?.id.equals(other.tak?.id)
                && serviceComponent?.id.equals(other.serviceComponent?.id)
                && serviceContract?.id.equals(other.serviceContract?.id)
                && logicalAddress?.id.equals(other.logicalAddress?.id))
    }

    @Override
    public int hashCode() {
        final int prime = 31;

        return ((prime + tak?.id)
                * (prime + serviceComponent?.id)
                * (prime + serviceContract?.id)
                * (prime + logicalAddress?.id));
    }
}
