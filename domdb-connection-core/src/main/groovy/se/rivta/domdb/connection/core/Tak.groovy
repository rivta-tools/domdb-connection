package se.rivta.domdb.connection.core

import javax.persistence.*


@Entity
class Tak {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // db generated id
    int id

    @Column(nullable = false)
    String name

    @OneToMany(mappedBy="tak")
    Set<ServiceComponent> serviceComponents
}
