OBS! Repositoryt är deprecated! 20211126
Connection-databasen innehåller exporterad information ur olika tjänsteadresseringskataloger, och består av följande entiteter:

![Connections.png](docs/images/Connections.png)

| **tabell** | **beskrivning** |
|:-----------|:----------------|
|tak         |De tjänsteadresseringskataloger som förekommer i databasen, kan vara t.ex. "NTjP Prod" och "NTjP QA"|
|service\_component| de olika tjänstekomponenter (konsumenter och producenter) som förekommer i en specifik tak |
| logical\_address |de logiska adresser som förekommer i en specifik tak |
| service\_contract | de tjänstekontrakt som förekommer i en specifik tak. Denna databas innehåller enbart dess namespace, mer detaljerad information finns i domändatabasen |
| routing    | anger vilka tjänstekomponenter som producerar vilka tjänstekontrakt för vilka logiska adresser |
| call\_authorization | anger vilka tjänstekonsumenter som får konsumera vilka tjänstekontrakt från vilka logiska adresser |

### Installation
För installationsinstruktioner av ovanstående, se: 

 * [HowTo](docs/HowTo.md).
 * [ConnectionConfiguration](docs/ConnectionConfiguration.md).
 * [ConnectionUpgrade.md](docs/ConnectionUpgrade.md)
 * [ImportTakData.md](docs/ImportTakData.md)

