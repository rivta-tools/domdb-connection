För att kunna göra detta behöver anslutningsdatabasen ha skapats och konfigurerats, se [ConnectionConfiguration](ConnectionConfiguration.md).

## Förutsättningar
På den server som innehåller programvarorna, ställ dig i den mapp som innehåller de uppackade filerna från domdb-connection-x.y.z.zip. Kopiera sedan dit den/de csv-filer du vill importera (ska vara i formatet "NEW" om du har flera versioner).

Du behöver även känna till de databas-id:n till de tak:ar som lades upp i anslutningsdatabasen i steget [ConnectionConfiguration](ConnectionConfiguration.md). Vanligtvis har produktionsmiljön ID=1 och QA-miljön ID=2.

## Importera
Importera en fil genom att köra följande kommando:

`groovy importSkltpConnectionFile.groovy <dumpfil> <takid>`

För närvarande måste sökvägen till dumpfilen skrivas som en absolut sökväg, t.ex. **~/domdb/domdb-connection-1.0.0/takutdrag\_new\_QA\_v08.csv**

exempel:

`groovy importSkltpConnectionFile.groovy ~/domdb/domdb-connection-1.0.0/takutdrag_new_QA_v08.csv 2`

## Alternativ, Använd Tak-API för att importera takdata.
Importera takdata genom att använda Tak-API

`groovy importSkltpConnectionTakAPI.groovy <tak-api-url> <platform> <environment> 2`

URL:en måste peka på roten där operationen defineras, tex connectionPoints?environment=QA

exempel:

`groovy importSkltpConnectionTakAPI.groovy http://api.ntjp.se/coop/api/v1/ NTJP QA 2`