## Zip-fil
Till att börja med behövs en zip-fil för den release du vill installera. Oftast finns dessa att hämta via länken **Releases** på förstasidan här på Google Code.

Ladda ner zip-filen och kopiera den till din domdb-mapp på servern. Packa sedan upp den med följande kommando:

```
unzip domdb-connection-x.y.z.zip
```

Ställ dig sedan i den mapp som innehåller de uppackade filerna.

## Installera binärfiler
Kör följande kommando för att uppdatera den lokala Maven-cachen med nya jar-filer:

```
./install.sh
```

## Uppdatera databas
Kör följande script för att säkerställa att databasen har alla tabeller och kolumner som behövs:

```
groovy verifyConnectionDB.groovy
```

## Nya script
Observera att den nya mappen även innehåller nya versioner av groovy-scripten, t.ex. importSkltpConnectionFile.groovy.
Det är viktigt att använda rätt version av scripten, då de innehåller referenser till specifika versioner av jar-filerna.