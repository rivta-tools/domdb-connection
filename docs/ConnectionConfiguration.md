Denna instruktion beskriver hur Connection-komponenterna installeras. För information om vad detta innehåller, se [startsidan](Home.md).

## Common-komponenter
Connection-komponenterna är beroende av viss gemensam konfiguration. Se [CommonConfiguration](CommonConfiguration.md)

## Databaser
För connection-core (anslutningsdatabasen) behövs en tom mysql-databas, förslagsvis kallas **Connections**.

### Databaskonfiguration

I den konfigurationsmapp som skapades i steget [CommonConfiguration](CommonConfiguration.md) (t.ex. **/etc/domdb**), skapa filen **connections-jpa.properties**. Lägg till följande innehåll i filen:

```
javax.persistence.jdbc.url=jdbc:mysql://<servernamn>:3306/<databasnamn>
javax.persistence.jdbc.user=<användare>
javax.persistence.jdbc.password=<lösenord>
```

## Installation av programvara
Kopiera och packa upp den senaste domdb-connection-x.y.z-bin.zip i den programmapp som skapades i steget [CommonConfiguration](CommonConfiguration.md), t.ex. **~/domdb**.

`unzip domdb-connection-x.y.z-bin.zip`

Kör sedan scriptet **install.sh** bland de uppackade filerna. Scriptet installerar domdb-connection-core-x.y.z.jar och domdb-connection-integration-x.y.z.jar i den lokala Maven-cachen.

`. install.sh`

## Skapa eller uppdatera databas
När ovanstående programvara installerats, och konfigurationsfilen **datasource-connections.properties** har skapats kan tabellerna i Connection-databasen skapas.

Kontrollera först att miljövariabeln **domdb\_config\_dir** är tillgänglig.

`echo $domdb_config_dir`

Sökvägen till konfigurationsmappen ska skrivas ut på skärmen, om inte kontrollera [CommonConfiguration](CommonConfiguration.md).

När allt ovanstående är gjort, ställ dig i den uppackade mappen domdb-connection-x.y.x och kör följande script:

`groovy verifyConnectionDB.groovy`

Scriptet laddar först ner en del programfiler från Maven Central (i tysthet). Därefter skapas tabeller i databasen, med hjälp av Hibernate. Scriptet kan köras även om databasen har innehåll, t.ex. vid uppgraderingar, och skapar då de objekt som ev. saknas.

## Skapa Tak-poster i databasen
För att anslutningsinformation för en specifik tjänsteadresseringskatalog senae ska kunna importeras, behöver respektive Tak:ar läggas upp i tabellen **tak** i anslutningsdatabasen. Detta är en engångsinsats och görs därför inte av några script.

Tak-tabellen består av två kolumner, **id** och **name**. Vissa komponenter, t.ex. **publicweb** förlitar sig på att en produktionsmiljö ska ha id=1 och att en QA-miljö ska ha id=2.

Anslut till mysql, t.ex. via kommandoprompten som beskrivs i [MySQLCheatSheet](MySQLCheatSheet.md) och lägg sedan in din produktions-tak och din qa-tak. Om du gör det via kommandoprompten kan du använda följande insert-satser:

```
use Connections;
INSERT INTO tak (id, name) VALUES (1, 'Prod');
INSERT INTO tak (id, name) VALUES (2, 'QA');
```

### Nästa steg
Nu kan du gå vidare till t.ex. [ImportTakData](ImportTakData.md).