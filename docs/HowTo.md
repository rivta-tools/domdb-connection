# Utvecklarinstruktioner

Detta är ett s.k. Maven "multi module project", med ett "parent project" (domdb-connection) och följande "sub-modules":

* domdb-connection-core - innehåller persistens-klasser för att representera anslutningsinformation. Innehåller även script för att skapa och uppdatera databastabeller.
* domdb-connection-integration - innehåller kod och script för att uppdatera anslutningsdatabasen utifrån en tak-dump.
* domdb-connection-package - innehåller Maven-konfiguration för att skapa en release-zip med jar-filer, groovy-script m.m.

För att underlätta så anges versionen av de flesta dependencies under "dependencyManagement" i parent pom.

## Programvaror
Du behöver följande programvaror installerade för att kunna köra koden i "utvecklarläge", samt för att paketera releaser:

* Java Development Kit (version 1.7+) - behöver vara installerad och miljövariabeln JAVA_HOME satt till JDK-installationens rotkatalog (ett steg ovanför javas bin-katalog)
* Groovy (version 2.4+) - behöver vara uppackad och dess bin-katalog finnas med i PATH
* Maven (version 3+) - behöver vara uppackas och dess bin-katalog finnas med i PATH
* MySQL (version 5.5+) - behöver vara installerad och du ha ett konto med hög behörighet

## Konfigurera utvecklingsmiljön
### Klona Git-repository
Klona detta git-repository via en Git-klient, alternativt ladda ned källkoden som en zip.

###Miljö variabel
**domdb_config_dir** måste konfigureras innan denna steg. 

### Skapa databaser
Skapa följande databaser i MySQL. Databasernas namn är inte viktigt, det anges via konfigurationsfiler sedan.

#### Connections
Denna databas kommer att innehålla information om anslutningar. 

### Skapa miljövariabel
Skapa en katalog för domdb konfigurationsfiler någonstans, och peka ut sökvägen via miljövariabeln **domdb_config_dir**.

### Skapa inställningsfiler
Skapa följande inställningsfiler i katalogen:
#### connections-jpa.properties 
Denna konfigurationsfil används av Groovy-scripten, och pekar ut sökvägen till anslutningsdatabasen

```
javax.persistence.jdbc.url=jdbc:mysql://<servernamn>:3306/<databasnamn>
javax.persistence.jdbc.user=<användare>
javax.persistence.jdbc.password=<lösenord>
```

## Köra programvaran
Innan någon av komponenterna kan exekveras så behöver jar-filerna installeras i din lokala Maven-cache. Vissa utvecklingsverktyg kan göra att detta steg inte behövs.

1. Starta en kommandoprompt och gå till domdb-connection (rotkatalogen i det klonade git-repositoriet)
2. Installera jar-filerna så att de hittas av scriptet: mvn install

### Förbered databasen
Gör så här för att skapa nödvändiga tabeller i databasen:

1. Starta en kommandoprompt och gå till domdb-connection/domdb-connection-core/src/main/scripts
2. Kör kommandot groovy verifyConnectionDb.groovy

Om jar-filerna är installerade och miljövariabeln och konfigurationsscriptet är korrekt så ska nu tabellerna skapas i databasen.

#### Lägg till tak-id:n
Lägg till följande poster i tak-tabellen:

id | name
-- | ----
1  | NTjP prod
2  | NTjP QA

Det är viktigt för public-projektet att prod-miljön har id 1.

### Integration-projektet
Nu ska du kunna köra import-scriptet.

1. Starta en kommandoprompt och gå till domdb-connection-integration/src/main/scripts
2. Kör kommandot groovy importSkltpConnectionFile.groovy *filnamn* *tak-id*

## Import via TAK-API istället
1. Starta en kommandoprompt och gå till domdb-connection-integration/src/main/scripts
2. Kör kommandot groovy importSkltpConnectionTakAPI.groovy *takAPIurl* *miljövariabel-i-TakAPI* *tak-id*
	ex. "groovy importSkltpConnectionTakAPI.groovy http://<url>/coop/api/v1/ 4 2"
	JAVA_OPTS kan behöva sättas till "-Xmx512m"

## Skapa en release
Så här gör du för att skapa en ny release. 

### Uppdatera versionsnummer
Versionsnumret behöver uppdateras i alla pom-filer. Det görs enklast med följande kommando:

´´´
cd <projektets rotkatalog>
mvn versions:set -DgenerateBackupPoms=False -DnewVersion=x.y.z
´´´

pom.xml i rotkatalogen ska nu ha fått det nya versionsnumret (under "version") och under-modulerna ska ha fått samma versionsnummer (under "parent/version").

Om det är viktigt att något script använder exakt denna nya version, uppdatera versionsnumret manuellt i Grapes/Grab-avsnittet längst upp i respektive script.

### Bygga release
Nu kan du bygga en release-zip med följande kommando:

´´´
mvn package
´´´

zip-filen skapas i katalogen domdb-connection-package/target

### Checka in ändringar
Om allt ser bra ut:

* committa dina ändringar till Git, 
* skapa en tag med samma namn som versionsnumret
* pusha alltsammans till Bitbucket (dubbelkolla att taggen syns på Bitbucket).
