This module creates a zip file with project jars, poms and an install script.

Run with "mvn package" on the parent project. The zip is created in target directory in this module.