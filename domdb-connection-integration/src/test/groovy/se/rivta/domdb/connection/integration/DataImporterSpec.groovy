package se.rivta.domdb.connection.integration

import se.rivta.domdb.connection.core.LogicalAddress
import se.rivta.domdb.connection.core.ServiceComponent
import se.rivta.domdb.connection.core.ServiceContract
import se.rivta.domdb.connection.core.Tak
import spock.lang.Specification

import javax.persistence.EntityManager
import javax.persistence.Query


class DataImporterSpec extends Specification {
    def tak = new Tak(id: 1)
    def entityManager = Mock(EntityManager)
    def producerQuery = Mock(Query)
    def consumerQuery = Mock(Query)
    def serviceContractQuery = Mock(Query)
    def logicalAddressQuery = Mock(Query)
    def routingQuery = Mock(Query)
    def callAuthorizationQuery = Mock(Query)

    final String consumerId = "consumerid"
    final String producerId = "producerid"
    final String logicalAddress = "logicalid"
    final String serviceContractName = "urn:riv:crm:1"

    def setup() {
        entityManager.find(Tak.class, 1) >> tak
        entityManager.createQuery({ it.contains("ServiceComponent") }) >>> [ producerQuery, consumerQuery ]
        entityManager.createQuery({ it.contains("ServiceContract") }) >> serviceContractQuery
        entityManager.createQuery({ it.contains("LogicalAddress") }) >> logicalAddressQuery
        entityManager.createQuery({ it.contains("Routing") }) >> routingQuery
        entityManager.createQuery({ it.contains("CallAuthorization") }) >> callAuthorizationQuery

        // These are managed by the CacheFilter class and not tested here
        routingQuery.getResultList() >> []
        callAuthorizationQuery.getResultList() >> []
    }

    void "Old producer/consumer/servicecontract/logicaladdress are not persisted again"() {
        setup:
        def dataImporter = new DataImporter(entityManager, tak.id)

        producerQuery.getResultList() >> [ new ServiceComponent(id: 10, hsaId: producerId)]
        consumerQuery.getResultList() >> [ new ServiceComponent(id: 20, hsaId: consumerId) ]
        serviceContractQuery.getResultList() >> [ new ServiceContract(id: 30, namespace: serviceContractName) ]
        logicalAddressQuery.getResultList() >> [ new LogicalAddress(id: 40, address: logicalAddress) ]

        when:
        dataImporter.importRelation(new DataItem(consumerId: consumerId, producerId: producerId, producerUrl: "http://test", serviceContract: serviceContractName, logicalAddress: logicalAddress))

        then:
        0 * entityManager.persist(_ as ServiceComponent)
        0 * entityManager.persist(_ as ServiceContract)
        0 * entityManager.persist(_ as LogicalAddress)
    }

    void "New producer is persisted"() {
        setup:
        def dataImporter = new DataImporter(entityManager, tak.id)

        producerQuery.getResultList() >> []
        consumerQuery.getResultList() >> [ new ServiceComponent(id: 20, hsaId: consumerId) ]
        serviceContractQuery.getResultList() >> [ new ServiceContract(id: 30, namespace: serviceContractName) ]
        logicalAddressQuery.getResultList() >> [ new LogicalAddress(id: 40, address: logicalAddress) ]

        when:
        dataImporter.importRelation(new DataItem(consumerId: consumerId, producerId: producerId, producerUrl: "http://test", serviceContract: serviceContractName, logicalAddress: logicalAddress))

        then:
        1 * entityManager.persist({ sc -> sc instanceof ServiceComponent && sc.hsaId == producerId && sc.tak == tak })
    }

    void "New consumer is persisted"() {
        setup:
        def dataImporter = new DataImporter(entityManager, tak.id)

        producerQuery.getResultList() >> [ new ServiceComponent(id: 10, hsaId: producerId)]
        consumerQuery.getResultList() >> []
        serviceContractQuery.getResultList() >> [ new ServiceContract(id: 30, namespace: serviceContractName) ]
        logicalAddressQuery.getResultList() >> [ new LogicalAddress(id: 40, address: logicalAddress) ]

        when:
        dataImporter.importRelation(new DataItem(consumerId: consumerId, producerId: producerId, producerUrl: "http://test", serviceContract: serviceContractName, logicalAddress: logicalAddress))

        then:
        1 * entityManager.persist({ sc -> sc instanceof ServiceComponent && sc.hsaId == consumerId && sc.tak == tak })
    }

    void "New service contract is persisted"() {
        setup:
        def dataImporter = new DataImporter(entityManager, tak.id)

        producerQuery.getResultList() >> [ new ServiceComponent(id: 10, hsaId: producerId)]
        consumerQuery.getResultList() >> [ new ServiceComponent(id: 20, hsaId: consumerId) ]
        serviceContractQuery.getResultList() >> []
        logicalAddressQuery.getResultList() >> [ new LogicalAddress(id: 40, address: logicalAddress) ]

        when:
        dataImporter.importRelation(new DataItem(consumerId: consumerId, producerId: producerId, producerUrl: "http://test", serviceContract: serviceContractName, logicalAddress: logicalAddress))

        then:
        1 * entityManager.persist({ sc -> sc instanceof ServiceContract && sc.namespace == serviceContractName && sc.tak == tak })
    }

    void "New logical address is persisted"() {
        setup:
        def dataImporter = new DataImporter(entityManager, tak.id)

        producerQuery.getResultList() >> [ new ServiceComponent(id: 10, hsaId: producerId)]
        consumerQuery.getResultList() >> [ new ServiceComponent(id: 20, hsaId: consumerId) ]
        serviceContractQuery.getResultList() >> [ new ServiceContract(id: 30, namespace: serviceContractName) ]
        logicalAddressQuery.getResultList() >> []

        when:
        dataImporter.importRelation(new DataItem(consumerId: consumerId, producerId: producerId, producerUrl: "http://test", serviceContract: serviceContractName, logicalAddress: logicalAddress))

        then:
        1 * entityManager.persist({ la -> la instanceof LogicalAddress && la.address == logicalAddress && la.tak == tak })
    }
}
