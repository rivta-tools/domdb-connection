package se.rivta.domdb.connection.integration

class SkltpFileParserTest extends GroovyTestCase {

    def serviceContracts = [
            ["identity": "urn:riv:contract1"]
    ]

    def consumers = [
            ["identity": "consumer1", "description": "abc123"],
            ["identity": "consumer2", "description": "cde123"],
    ]

    def logicaladdresses = [
            ["identity": "address1", description: "def456" ],
            ["identity": "address2", description: "efg456" ],
            ["identity": "address3", description: "ggg456" ]
    ]

    def producers = [
            ["identity": "producer1", description: "ghi789", "url": "www.aaa.com/u/r/l" ],
            ["identity": "producer2", description: "efg789", "url": "www.efg.com/u/r/l" ]
    ]

    void testParse_oneRow(){

        String[] row1 = serviceContracts[0].values() + consumers[0].values() + logicaladdresses[0].values() + producers[0].values()

        String contents = "Header row\n" + row1.join('\t') + "\n"
        byte[] bytes = contents.getBytes("UTF-8")
        def stream = new ByteArrayInputStream(bytes)
        def parser = new SkltpFileParser()

        List<DataItem> relations = parser.parseInputFile(stream)

        assert relations.size() == 1
        DataItem relation = relations[0]

        assertContract(relation, serviceContracts[0])
        assertProducer(relation, producers[0])
        assertConsumer(relation, consumers[0])
        assertLogicalAddress(relation, logicaladdresses[0])
    }

    void testParse_hasEmptyRows(){

        String[] row1 = serviceContracts[0].values() + consumers[0].values() + logicaladdresses[0].values() + producers[0].values()

        String contents = "Header row\n\n" + row1.join('\t') + "\n\n" // <-- Two line breaks
        byte[] bytes = contents.getBytes("UTF-8")
        def stream = new ByteArrayInputStream(bytes)
        def parser = new SkltpFileParser()

        List<DataItem> relations = parser.parseInputFile(stream)

        assert relations.size() == 1
        DataItem relation = relations[0]

        assertContract(relation, serviceContracts[0])
        assertProducer(relation, producers[0])
        assertConsumer(relation, consumers[0])
        assertLogicalAddress(relation, logicaladdresses[0])
    }

    void testParse_twoConsumers(){

        def row1 = serviceContracts[0].values() + consumers[0].values() + logicaladdresses[0].values() + producers[0].values()
        def row2 = serviceContracts[0].values() + consumers[1].values() + logicaladdresses[0].values() + producers[0].values()

        String contents = "Header row\n" + row1.join('\t') + "\n" + row2.join('\t') + "\n"
        byte[] bytes = contents.getBytes("UTF-8")
        def stream = new ByteArrayInputStream(bytes)
        def parser = new SkltpFileParser()

        List<DataItem> relations = parser.parseInputFile(stream)

        assert relations.size() == 2

        assertContract(relations[0], serviceContracts[0])
        assertProducer(relations[0], producers[0])
        assertConsumer(relations[0], consumers[0])
        assertLogicalAddress(relations[0], logicaladdresses[0])

        assertContract(relations[1], serviceContracts[0])
        assertProducer(relations[1], producers[0])
        assertConsumer(relations[1], consumers[1])
        assertLogicalAddress(relations[1], logicaladdresses[0])
    }

    private void assertContract(DataItem relation, Map dataMap){
        assert relation.serviceContract == dataMap["identity"]
    }

    private void assertProducer(DataItem relation, Map dataMap){
        assert relation.producerId == dataMap["identity"]
        assert relation.producerDescription == dataMap["description"]
        assert relation.producerUrl == dataMap["url"]
    }

    private void assertLogicalAddress(DataItem relation, Map dataMap){
        assert relation.logicalAddress == dataMap["identity"]
        assert relation.logicalAddressDescription == dataMap["description"]
    }

    private void assertConsumer(DataItem relation, Map dataMap){
        assert relation.consumerId == dataMap["identity"]
        assert relation.consumerDescription == dataMap["description"]
    }
}
