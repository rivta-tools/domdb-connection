package se.rivta.domdb.connection.integration

import se.rivta.domdb.connection.core.LogicalAddress
import se.rivta.domdb.connection.core.Routing
import se.rivta.domdb.connection.core.ServiceComponent
import se.rivta.domdb.connection.core.ServiceContract
import se.rivta.domdb.connection.core.Tak
import spock.lang.Specification

import javax.persistence.EntityManager
import javax.persistence.Query


class CacheFilterSpec extends Specification {

    void "Objects in database are not persisted again"() {
        setup:
        def entityManager = Mock(EntityManager)
        def query = Mock(Query)

        entityManager.createQuery(_) >> query

        def routing = new Routing(serviceComponent: new ServiceComponent(id: 3), serviceContract: new ServiceContract(id: 4, namespace: "urn:riv:contract"), logicalAddress: new LogicalAddress(id: 9), tak: new Tak(id: 2))
        query.getResultList() >> [ routing ]

        when:
        def cache = new CacheFilter(entityManager, "Routing", 1)
        cache.persistIfNotExists(routing)

        then:
        0 * entityManager.persist(routing)
    }

    void "Objects not in database are persisted, but only once"() {
        setup:
        def entityManager = Mock(EntityManager)
        def query = Mock(Query)

        entityManager.createQuery(_) >> query

        def routing1 = new Routing(serviceComponent: new ServiceComponent(id: 3), serviceContract: new ServiceContract(id: 4, namespace: "urn:riv:contract"), logicalAddress: new LogicalAddress(id: 9), tak: new Tak(id: 2))
        def routing2 = new Routing(serviceComponent: new ServiceComponent(id: 5), serviceContract: new ServiceContract(id: 7, namespace:  "urn:riv:contract"), logicalAddress: new LogicalAddress(id: 9), tak: new Tak(id: 2))

        query.getResultList() >> [ routing1 ]

        when:
        def cache = new CacheFilter(entityManager, "Routing", 1)
        cache.persistIfNotExists(routing2)
        cache.persistIfNotExists(routing2)

        then:
        1 * entityManager.persist(routing2)
    }

    void testDeleteOldRoutingsAndCallAuthorizations() {

    }
}
