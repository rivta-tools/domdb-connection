package se.rivta.domdb.connection.integration


class DataItem {
    def serviceContract
    def producerId
    def producerDescription
    def producerUrl
    def logicalAddress
    def logicalAddressDescription
    def consumerId
    def consumerDescription
}
