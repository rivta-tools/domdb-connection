package se.rivta.domdb.connection.integration

import groovy.util.logging.Log
import se.rivta.domdb.connection.core.CallAuthorization
import se.rivta.domdb.connection.core.LogicalAddress
import se.rivta.domdb.connection.core.Routing
import se.rivta.domdb.connection.core.ServiceComponent
import se.rivta.domdb.connection.core.ServiceContract
import se.rivta.domdb.connection.core.Tak

import javax.persistence.Query


@Log
class DataImporter {
    private entityManager
    private Tak tak
    private CacheFilter callAuthorizationCache
    private CacheFilter routingCache
    private Map<String,ServiceComponent> serviceComponents
    private Map<String,ServiceContract> serviceContracts
    private Map<String,LogicalAddress> logicalAddresses

    DataImporter(entityManager, int takId){
        this.entityManager = entityManager
        this.tak = retrieveTakObject(takId)
        this.routingCache = new CacheFilter(entityManager, "Routing", tak.id)
        this.callAuthorizationCache = new CacheFilter(entityManager, "CallAuthorization", tak.id)
        this.serviceComponents = new HashMap<String,ServiceComponent>()
        this.serviceContracts = new HashMap<String,ServiceContract>()
        this.logicalAddresses = new HashMap<String,LogicalAddress>()
    }

    private Tak retrieveTakObject(int takId) {
        Tak tak = entityManager.find(Tak.class, takId)

        if (!tak) {
            // Without tak object we are nothing...
            throw new Exception("Tak with id ${takId} not found in database.")
        }

        return tak
    }

    public void importRelation(DataItem relation){
        try {
            ServiceComponent producer = updateAndReturnServiceComponent(relation.producerId, relation.producerDescription)
            ServiceComponent consumer = updateAndReturnServiceComponent(relation.consumerId, relation.consumerDescription)
            ServiceContract serviceContract = updateAndReturnServiceContract(relation.serviceContract)
            LogicalAddress logicalAddress = updateAndReturnLogicalAddress(relation.logicalAddress, relation.logicalAddressDescription)

            updateRouting(producer, serviceContract, logicalAddress)
            updateCallAuthorization(consumer, serviceContract, logicalAddress)

        } catch (Exception ex) {
            throw new Exception("Failed to import: ${relation}", ex)
        }
    }

    public void deleteOldRoutingsAndCallAuthorizations(){
        routingCache.deleteUnusedObjectsInDatabase()
        callAuthorizationCache.deleteUnusedObjectsInDatabase()
    }

    private void updateRouting(ServiceComponent producer, ServiceContract serviceContract, LogicalAddress logicalAddress) {
        def routing = new Routing(
                serviceComponent: producer,
                serviceContract: serviceContract,
                logicalAddress: logicalAddress,
                tak: this.tak
        )

        routingCache.persistIfNotExists(routing)
    }

    private void updateCallAuthorization(ServiceComponent consumer, ServiceContract serviceContract, LogicalAddress logicalAddress) {
        def authorization = new CallAuthorization(
                serviceComponent: consumer,
                serviceContract: serviceContract,
                logicalAddress: logicalAddress,
                tak: this.tak
        )

        callAuthorizationCache.persistIfNotExists(authorization)
    }

    private ServiceComponent updateAndReturnServiceComponent(String identity, String description) {
        ServiceComponent serviceComponent = serviceComponents.get(identity)

        if (!serviceComponent) {
            final String queryString = "select c from ServiceComponent c where c.hsaId=:hsaId and c.tak.id=:takId";
            def queryParameters = ["hsaId": identity, "takId": this.tak.id]
            def createFunction = { return new ServiceComponent(hsaId: identity, tak: tak) }

            serviceComponent = retrieveOrCreateObject(queryString, queryParameters, createFunction)
            serviceComponent.description = description
            serviceComponents.put(identity, serviceComponent)
        }

        return serviceComponent
    }

    private LogicalAddress updateAndReturnLogicalAddress(String identity, String description) {
        LogicalAddress logicalAddress = logicalAddresses.get(identity)

        if (!logicalAddress) {
            final String queryString = "select a from LogicalAddress a where a.address=:address and a.tak.id=:takId"

            def queryParameters = ["address": identity, "takId": this.tak.id]
            def createFunction = { return new LogicalAddress(address: identity, tak: this.tak) }

            logicalAddress = retrieveOrCreateObject(queryString, queryParameters, createFunction)
            logicalAddress.description = description

            logicalAddresses.put(identity, logicalAddress)
        }

        return logicalAddress
    }

    public ServiceContract updateAndReturnServiceContract(String namespace) {
        ServiceContract serviceContract = serviceContracts.get(namespace)

        if (!serviceContract) {
            final String queryString = "select c from connection.ServiceContract c where c.namespace=:namespace and c.tak.id=:takId";
            def queryParameters = ["namespace": namespace, "takId": this.tak.id]
            def createFunction = { return new ServiceContract(namespace: namespace, tak: tak) }
            serviceContract = retrieveOrCreateObject(queryString, queryParameters, createFunction)

            serviceContracts.put(namespace, serviceContract)
        }

        return serviceContract
    }

    private def retrieveOrCreateObject(String queryString, Map queryParameters, Closure createFunction){
        def object

        Query query = entityManager.createQuery(queryString);
        queryParameters.each {
            query.setParameter(it.key, it.value)
        }

        List results = query.getResultList()

        if (!results.isEmpty()) {
            log.finer("Object found in database")
            object = results.get(0)
        } else {
            log.info("Creating new object.")
            object = createFunction()

            entityManager.persist(object)
        }

        return object
    }
}
