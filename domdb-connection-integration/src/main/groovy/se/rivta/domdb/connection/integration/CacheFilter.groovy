package se.rivta.domdb.connection.integration

import groovy.util.logging.Log

import javax.persistence.Query

/**
 * Imports all entities of type "className" into an internal dictionary
 * Persists only entities not found in the dictionary (new entities) to the entityManager
 * Also gives the possibility to delete unused objects
 */
@Log
class CacheFilter {
    private entityManager
    private Map cache
    private String entityName
    private int takId

    CacheFilter(entityManager, entityName, int takId){
        this.entityManager = entityManager
        this.entityName = entityName
        this.takId = takId
    }

    public void persistIfNotExists(entity) {
        log.finer("Querying for routing.")

        if (cache == null){
            createCache(entityName)
        }

        if (cache.containsKey(entity) == false) {
            log.fine("Storing object.")

            entityManager.persist(entity)
        } else {
            log.finer("Object found.")
        }

        // Set the item value to null to indicate that the object exist in database
        // and was used during the import
        cache[entity] = true
    }

    /**
     * Delete objects in the database that was not seen by the persistIfNotExists method
     */
    public void deleteUnusedObjectsInDatabase(){
        def unusedObjects = cache.findAll { it.value == false}

        if (unusedObjects.size() == 0) {
            log.info("No objects to delete from ${entityName}.")
        } else {
            log.info("Deleting ${unusedObjects.size()} objects from ${entityName}.")

            unusedObjects.each {
                entityManager.remove(it.key)
            }
        }
    }

    private void createCache(String entityName) {
        log.info("Filling cache for: ${entityName}")
        cache = new HashMap()

        Query query = entityManager.createQuery("select o from ${entityName} o where o.tak.id=:takId");
        query.setParameter("takId", this.takId)
        List results = query.getResultList()

        for(result in results) {
            cache[result] = false
        }

        log.info("Cache filled with ${cache.size()} entities")
    }
}
