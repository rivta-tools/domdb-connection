package se.rivta.domdb.connection.integration

import groovy.util.logging.Log

import java.nio.charset.Charset
import java.nio.charset.StandardCharsets

/**
 * Parses csv dump file from Skltp tjänsteadresseringskatalog
 */
@Log
class SkltpFileParser {

    private final String columnSeparator = '\t'

    private class columns {
        static short contractId = 0
        static short consumerId = 1
        static short consumerDescription = 2
        static short logicalAddressId = 3
        static short logicalAddressDescription = 4
        static short producerId = 5
        static short producerDescription = 6
        static short producerUrl = 7
    }

    public Charset charSet = StandardCharsets.ISO_8859_1

    List<DataItem> parseInputFile(InputStream stream){
        def dataItems = new ArrayList<DataItem>()
        List<String[]> rows = createDataRows(stream)

        for(String[] row in rows){
            if (row.size() < 8) {
                log.warning("Skipping row: " + row)
                continue
            }

            def dataItem = new DataItem()
            dataItem.producerId = row[columns.producerId]
            dataItem.producerUrl = row[columns.producerUrl]
            dataItem.producerDescription = row[columns.producerDescription]
            dataItem.serviceContract = row[columns.contractId]

            dataItem.consumerId = row[columns.consumerId]
            dataItem.consumerDescription = row[columns.consumerDescription]

            dataItem.logicalAddress = row[columns.logicalAddressId]
            dataItem.logicalAddressDescription = row[columns.logicalAddressDescription]

            dataItems.add(dataItem)
        }

        return dataItems
    }

    private List<String[]> createDataRows(InputStream stream) {
        BufferedReader reader = stream.newReader(charSet.name())
        reader.readLine() // Throw away header line

        List<String[]> rows = reader.readLines().collect { it.split(columnSeparator) }
        return rows
    }
}
