#!/usr/bin/env groovy

package groovy

import groovy.json.JsonSlurper
import groovy.transform.Field
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.bridge.SLF4JBridgeHandler
@Grapes([
        @Grab(group = 'se.rivta.domdb.connection', module = 'domdb-connection-core', version = '[1.2,)'),
        @Grab(group = 'se.rivta.domdb.connection', module = 'domdb-connection-integration', version = '[1.2,)'),
        @Grab(group = 'org.hibernate', module = 'hibernate-entitymanager', version = '4.3.7.Final'),
        @GrabExclude('xml-apis:xml-apis'), // Kills EntityManagerFactory with DocumentBuilderFactory error
        @Grab('mysql:mysql-connector-java:5.1.34'),
        @Grab(group = 'ch.qos.logback', module = 'logback-classic', version = '1.2.3'),
        @Grab(group = 'net.logstash.logback', module = 'logstash-logback-encoder', version = '6.4'),
        @Grab(group = 'org.slf4j', module = 'jul-to-slf4j', version = '1.7.22'),
        @Grab(group = 'org.apache.ivy', module = 'ivy', version = '2.4.0'),
        @GrabConfig(systemClassLoader = true)
])

import se.rivta.domdb.connection.integration.DataImporter
import se.rivta.domdb.connection.integration.DataItem

import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence
import java.nio.charset.StandardCharsets

SLF4JBridgeHandler.removeHandlersForRootLogger();
SLF4JBridgeHandler.install();

@Field
def Logger logger = LoggerFactory.getLogger("scriptLogger")

void importConnectionFile(String jsonURLString, String platform, String environment, int takId) {
    logger.info("Opening jsonURL ${jsonURLString} for tak id ${takId}")

    def jsonURLConnectionPointString = "${jsonURLString}connectionPoints.json?environment=${environment}&platform=${platform}"
    def jsonURLConnectionPoint = jsonURLConnectionPointString.toURL()
    def jsonConnectionPointAsText
    logger.info("Fetching connectionPointID...")
    jsonConnectionPointAsText = jsonURLConnectionPoint.getText(StandardCharsets.UTF_8.toString())
    def slurperConnectionPoint = new JsonSlurper().parseText(jsonConnectionPointAsText)
    int connectionPointId
    slurperConnectionPoint.each {
        connectionPointId = it.id
    }
    logger.info("Found connectionPointID " + connectionPointId)

    def jsonURLCoopString = "${jsonURLString}cooperations.json?connectionPointId=${connectionPointId}&include=connectionPoint%2ClogicalAddress%2CserviceContract%2CserviceConsumer"
    def jsonURLProducerString = "${jsonURLString}serviceProductions.json?connectionPointId=${connectionPointId}&include=connectionPoint%2ClogicalAddress%2CserviceContract%2CserviceProducer"
    def jsonURLInstalledContractsString = "${jsonURLString}installedContracts.json?connectionPointId=${connectionPointId}"
    def jsonURLCoop = jsonURLCoopString.toURL()
    def jsonURLProducer = jsonURLProducerString.toURL()
    def jsonURLInstalledContracts = jsonURLInstalledContractsString.toURL()
    def jsonCoopAsText
    def jsonProducerAsText
    def jsonInstalledContractsAsText

    List<DataItem> coopItems = new ArrayList<DataItem>()
    List<DataItem> producerItems = new ArrayList<DataItem>()
    List<DataItem> relations = new ArrayList<DataItem>()

    logger.info("Reading url...")
    logger.info("Reading ${jsonURLCoopString}")
    jsonCoopAsText = jsonURLCoop.getText(StandardCharsets.UTF_8.toString())
    logger.info("Reading ${jsonURLProducerString}")
    jsonProducerAsText = jsonURLProducer.getText(StandardCharsets.UTF_8.toString())
    logger.info("Reading ${jsonURLInstalledContractsString}")
    jsonInstalledContractsAsText = jsonURLInstalledContracts.getText(StandardCharsets.UTF_8.toString())

    logger.info("Parsing jsonText Coop...")
    def slurperCoop = new JsonSlurper().parseText(jsonCoopAsText)
    logger.info("Parsing jsonText Producer...")
    def slurperProducer = new JsonSlurper().parseText(jsonProducerAsText)
    logger.info("Parsing jsonText InstalledContracts...")
    def slurperInstalledContracts = new JsonSlurper().parseText(jsonInstalledContractsAsText)

    logger.info("Creating Coop objects...")
    slurperCoop.each {
        def dataItem = new DataItem()
        dataItem.producerId = ""
        dataItem.producerUrl = ""
        dataItem.producerDescription = ""
        dataItem.serviceContract = it.serviceContract.namespace

        dataItem.consumerId = it.serviceConsumer.hsaId
        dataItem.consumerDescription = it.serviceConsumer.description

        dataItem.logicalAddress = it.logicalAddress.logicalAddress
        dataItem.logicalAddressDescription = it.logicalAddress.description

        coopItems.add(dataItem)
    }

    logger.info("Creating Producer objects...")
    slurperProducer.each {
        def dataItem = new DataItem()
        dataItem.producerId = it.serviceProducer.hsaId
        dataItem.producerUrl = it.physicalAddress
        dataItem.producerDescription = it.serviceProducer.description
        dataItem.serviceContract = it.serviceContract.namespace

        dataItem.consumerId = ""
        dataItem.consumerDescription = ""

        dataItem.logicalAddress = it.logicalAddress.logicalAddress
        dataItem.logicalAddressDescription = it.logicalAddress.description

        producerItems.add(dataItem)
    }

    logger.info("Combining objects...")
    coopItems.each {
        def coopItem = it
        producerItems.each {
            if (coopItem.serviceContract.equals(it.serviceContract) &&
                    coopItem.logicalAddress.equals(it.logicalAddress)) {
                def dataItem = new DataItem()
                dataItem.producerId = it.producerId
                dataItem.producerUrl = it.producerUrl
                dataItem.producerDescription = it.producerDescription
                dataItem.serviceContract = it.serviceContract

                dataItem.consumerId = coopItem.consumerId
                dataItem.consumerDescription = coopItem.consumerDescription

                dataItem.logicalAddress = it.logicalAddress
                dataItem.logicalAddressDescription = it.logicalAddressDescription

                relations.add(dataItem)
            }
        }
    }

    logger.info("Number of combined objects: " + relations.size())

    logger.info("Importing to database...")
    def importer = new DataImporter(entityManager, takId)
    entityManager.getTransaction().begin()

    for (DataItem relation in relations) {
        importer.importRelation(relation)
    }

    logger.info("Adding Installed Contracts without relations...")
    slurperInstalledContracts.each {
        def contractItem = it.serviceContract.namespace
        boolean foundContract = false
        relations.each {
            if (contractItem.equals(it.serviceContract)) {
                foundContract = true
            }
        }
        if (!foundContract) {
            importer.updateAndReturnServiceContract(contractItem)
        }
    }

    importer.deleteOldRoutingsAndCallAuthorizations()

    entityManager.getTransaction().commit()

    logger.info("Finished importing relations.")
}

try {
    logger.info("ImportSkltpConnectionTakAPI: Start")
    if (args.size() < 4) {
        println("Arguments: <url> <connectionPointId> <takId>")
        println("- <url> the url to the cooperation api (tak-api). e.g. http://dev.api.ntjp.se/coop/api/v1/")
        println("- platform string for getting the connectionPointId in the cooperation api (tak-api)")
        println("- environment string for getting the connectionPointId in the cooperation api (tak-api)")
        println("- <takId> the tak id in the destination database")
        return
    }

    def jsonURL = args[0]
    def platform = args[1]
    def environment = args[2]
    def takId = args[3].toInteger()

    def configFileName = "${System.getenv("domdb_config_dir")}/connections-jpa.properties"
    def configStream = new FileInputStream(configFileName)
    def jpaProperties = new Properties()
    jpaProperties.load(configStream)

    EntityManagerFactory factory = Persistence.createEntityManagerFactory("datasource-connections", jpaProperties)

    entityManager = factory.createEntityManager()

    importConnectionFile(jsonURL, platform, environment, takId)

    factory.close()

    logger.info("ImportSkltpConnectionTakAPI: Success!")
} catch (Exception e) {
    logger.error("Exception in importSkltpConnectionTakAPI.groovy", e)
}