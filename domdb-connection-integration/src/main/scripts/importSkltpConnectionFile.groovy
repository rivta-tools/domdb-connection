#!/usr/bin/env groovy

package groovy

@Grapes([
        @Grab(group='se.rivta.domdb.connection', module='domdb-connection-core', version='[1.2,)'),
        @Grab(group='se.rivta.domdb.connection', module='domdb-connection-integration', version='[1.2,)'),
        @Grab(group='org.hibernate', module='hibernate-entitymanager', version='4.3.7.Final'),
        @GrabExclude('xml-apis:xml-apis'), // Kills EntityManagerFactory with DocumentBuilderFactory error
        @Grab('mysql:mysql-connector-java:5.1.34'),
        @Grab(group='org.apache.ivy', module='ivy', version='2.4.0'),
        @GrabConfig(systemClassLoader = true)
])

import se.rivta.domdb.connection.integration.DataImporter
import se.rivta.domdb.connection.integration.SkltpFileParser
import se.rivta.domdb.connection.integration.DataItem

import javax.persistence.EntityManagerFactory
import javax.persistence.Persistence
import java.nio.charset.Charset
import java.util.logging.Level
import java.util.logging.Logger


void setLoggingLevels(){
    // Disable logging about non-production connection pools and ehcache configuration...
    Logger.getLogger("org.hibernate").level = Level.SEVERE
}


void importConnectionFile(String filePath, int takId){
    println("Opening file ${filePath} for tak id ${takId}")
    def inputStream = new FileInputStream(filePath)
    List<DataItem> relations

    try {
        println("Parsing csv file...")
        relations = parser.parseInputFile(inputStream)
        println(relations.size() + " relations found")
    } finally {
        inputStream.close()
    }

    println("Importing to database...")
    def importer = new DataImporter(entityManager, takId)
    entityManager.getTransaction().begin()

    for (DataItem relation in relations) {
        importer.importRelation(relation)
    }

    importer.deleteOldRoutingsAndCallAuthorizations()

    entityManager.getTransaction().commit()

    println("Finished importing relations.")
}

if (args.size() < 2){
    println("Arguments: <filePath> <takId> [<charSet>]")
    println("- <filePath> must be an absolute path")
    return
}


def filePath = args[0]
def takId = args[1].toInteger()

setLoggingLevels()

def startTime = new Date()
println("Start time: ${startTime}")

def configFileName = "${System.getenv("domdb_config_dir")}/connections-jpa.properties"
def configStream = new FileInputStream(configFileName)
def jpaProperties = new Properties()
jpaProperties.load(configStream)

EntityManagerFactory factory = Persistence.createEntityManagerFactory("datasource-connections", jpaProperties)

entityManager = factory.createEntityManager()

// These object must not be defined with "def" - otherwise they will not be visible inside script methods
parser = new SkltpFileParser()

if (args.size() >= 3) {
    //parser.charSet = StandardCharsets.ISO_8859_1
    parser.charSet = Charset.forName(args[2])
}

importConnectionFile(filePath, takId)

factory.close()

println("Start time: ${startTime}. End time: ${new Date()}")